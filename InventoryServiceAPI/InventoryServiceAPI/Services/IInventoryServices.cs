﻿using InventoryServiceAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryServiceAPI.Services
{
   public interface IInventoryServices
    {
        public InventoryItems AddInventoryItems(InventoryItems items);
        public Dictionary<string, InventoryItems> GetInventoryItems();
    }
}
