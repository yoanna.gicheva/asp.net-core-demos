﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HELLAMEGARecordStore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HELLAMEGARecordStore.Controllers
{
    [ApiController]
    [Route("artists")]
    public class ArtistsController : ControllerBase
    {
        public List<Artist> Artists { get; set; } = new List<Artist>
        {
            new Artist
            {
                Id = 1,
                Name = "Meek Mill",
                Bio = "Meek Mill. Philadelphia, Pennsylvania, U.S. Robert Rihmeek Williams (born May 6, 1987), known professionally as Meek Mill, is an American rapper, songwriter, and activist. Born and raised in Philadelphia, he embarked on his music career as a battle rapper, and later formed a short-lived rap group, The Bloodhoundz.",
            },
            new Artist
            {
                Id = 2,
                Name = "Kendrick Lamar",
                Bio = "Kendrick Lamar Duckworth (born June 17, 1987) is an American rapper, songwriter, and record producer. He is regarded by many critics and contemporaries as one of the most important and influential hip hop artists of his generation, and as one of the greatest rappers of all time. Aside from his solo career, Lamar is also known as a member of the hip hop supergroup Black Hippy, alongside his TDE label-mates Ab-Soul, Jay Rock, and Schoolboy Q.",
            },
        };

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return Ok(this.Artists);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var artist = this.Artists.FirstOrDefault(x => x.Id == id);

            if (artist == null)
            {
                return NotFound();
            }

            return Ok(artist);
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post([FromBody] Artist model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            int id = Artists.Count() + 1;
            var artist = new Artist
            {
                Id = id,
                Name = model.Name,
                Bio = model.Bio,
            };

            //Does not work => controllers are resolved on each request - demo purposes only
            this.Artists.Add(artist);

            return Created("Post", artist);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(int id, [FromBody] Artist model)
        {
            if (id == 0 || model == null)
            {
                return BadRequest();
            }

            var artist = this.Artists.FirstOrDefault(x => x.Id == id);

            //Does not work => controllers are resolved on each request - demo purposes only
            artist.Name = model.Name;
            artist.Bio = model.Bio;

            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]

        public IActionResult Delete(int id)
        {
            if(id >= Artists.Count || id < 0)
            {
                return BadRequest();
            }

            //action
            Artists.RemoveAt(id);
            return Ok("Artist removed from list!");
        }



    }
}
