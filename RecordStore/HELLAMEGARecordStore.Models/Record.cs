﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HELLAMEGARecordStore.Models
{
    public class Record
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ArtistId { get; set; }
        public string Genre { get; set; }
    }
}
