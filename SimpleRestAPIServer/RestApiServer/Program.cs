using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace REST
{
    public class Program
    {
        public static void Main()
        {
            var host = Host
                .CreateDefaultBuilder()
                .ConfigureWebHostDefaults(configuration => configuration.UseStartup<Startup>())
                .Build();

            host.Run();
        }
    }
}
