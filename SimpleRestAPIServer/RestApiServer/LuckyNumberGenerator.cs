﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;

namespace REST.Controllers
{
    [ApiController]
    [Route("i-am-feeling-lucky")]
    // To test, open http://localhost:9999/i-am-feeling-lucky in the browser
    public class LuckyNumberGenerator : ControllerBase
    {
        public List<int> Numbers { get; set; } = new List<int>() { 4, 6 };

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var randomGenerator = new Random(DateTime.Now.Millisecond);
            int number = randomGenerator.Next(0, 10);
            if (Numbers.Contains(number))
            {
                return BadRequest($"The number {number} already exists!");
            }
            Numbers.Add(number);

            string num = number.ToString();
            return Ok(num);
        }

        [HttpGet]
        [Route("{index}")]
        public IActionResult Get(int index)
        {
            if (index > Numbers.Count || index < 0)
            {
                return BadRequest("The current id does not exist!");
            }
            return Ok(Numbers[index - 1]);
        }

        [HttpPost]
        public IActionResult Post([FromBody] int number)
        {
            if (Numbers.Contains(number))
            {
                return BadRequest($"The number {number} already exists!");
            }
            Numbers.Add(number);
            return Ok($"The number {number} was added successfully!");
        }

        [HttpPut]
        [Route("{index}")]
        public IActionResult Put([FromBody] int number, int index)
        {
            if(index > Numbers.Count || index < 0)
            {
                return BadRequest("There is no such existing index!");
            }
            if(Numbers.Contains(number))
            {
                return BadRequest("The number already exists!");
            }
            Numbers[index - 1] = number;

            return Ok("The number was updated correctly!");
        }

        [HttpDelete]
        [Route("index={index}")]
        public IActionResult Delete(int index)
        {
            if (index > Numbers.Count || index < 0)
            {
                return BadRequest("There is no such existing index!");
            }

            Numbers.RemoveAt(index - 1);
            return Ok("The number was removed successfully!");
        }
    }
}
